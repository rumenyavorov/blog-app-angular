import { FormGroup } from '@angular/forms';

export function matchingPasswords(pass: string, confirmPass: string) {
  return (group: FormGroup) => {
    const password = group.controls[pass];
    const confirmPassword = group.controls[confirmPass];

    if (password.errors || confirmPassword.errors) {
      return;
    }

    if (password.value !== confirmPassword.value) {
      confirmPassword.setErrors({ matchingPasswords: true });
    } else if (confirmPassword.value === '') {
      confirmPassword.setErrors({ required: true });
    }
  };
}
