import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RegisterComponent } from 'src/app/components/register/register.component';
import { CoreModule } from './core/core.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';
import { LoginComponent } from './components/login/login.component';
import { HomeComponent } from './components/home/home.component';
import { ToastrModule } from 'ngx-toastr';
import { JwtTokenInterceptor } from './core/interceptors/jwtToken-interceptor.service';
import { CreatePostComponent } from './components/post/create-post/create-post.component';
import { NotFoundComponent } from './shared/components/not-found/not-found.component';
import { MaterialModule } from './modules/material.module';
import { RouterModule } from '@angular/router';
import { SpinnerInterceptor } from './core/interceptors/spinner-interceptor.service';
import { NgxSpinnerModule } from 'ngx-spinner';
import { ProfileModule } from './components/profile/profile.module';
import { EditPostComponent } from './components/post/edit-post/edit-post.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { CommonModule } from '@angular/common';
import { SingleViewCommunityComponent } from './components/community/single-view-community/single-view-community.component';
import { SingleViewPostComponent } from './components/post/single-view-post/single-view-post.component';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { environment } from '../environments/environment';
import { ChatComponent } from './components/chat/chat.component';

@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    LoginComponent,
    HomeComponent,
    CreatePostComponent,
    NotFoundComponent,
    EditPostComponent,
    SingleViewCommunityComponent,
    SingleViewPostComponent,
    ChatComponent,
  ],
  imports: [
    CommonModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    CoreModule,
    HttpClientModule,
    ReactiveFormsModule,
    ToastrModule.forRoot({
      timeOut: 3000,
      positionClass: 'toast-bottom-right',
      preventDuplicates: true,
    }),
    FormsModule,
    MaterialModule,
    RouterModule,
    SharedModule,
    NgxSpinnerModule,
    ProfileModule,
    FlexLayoutModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFirestoreModule
    ],
  exports: [
    AppComponent,
    RegisterComponent,
    LoginComponent,
    HomeComponent,
    CreatePostComponent,
    NotFoundComponent,
    EditPostComponent,
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtTokenInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: SpinnerInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

