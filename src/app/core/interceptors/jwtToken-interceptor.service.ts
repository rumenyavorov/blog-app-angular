import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { AuthService } from '../services/auth.service';
import { StorageService } from '../services/storage.service';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { NotificationService } from '../services/notification.service';


@Injectable()
export class JwtTokenInterceptor implements HttpInterceptor {
  constructor(
    private readonly storageService: StorageService,
    private readonly authService: AuthService,
    private readonly router: Router,
    private readonly notificationService: NotificationService,
  ) {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler,
  ): Observable<HttpEvent<any>> {
    const token = this.storageService.getItem('token');

    if (token !== null && this.authService.isTokenExpired(token)) {
      this.router.navigateByUrl('/login');
      this.notificationService.errorMessage('Your session has expired!');
      this.authService.logOut();
    }
    req = req.clone({
      setHeaders: {
        authorization: `Bearer ${token}`,
      }
    });

    return next.handle(req);
  }
}
