import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserService } from './services/user.service';
import { AuthService } from './services/auth.service';
import { StorageService } from './services/storage.service';
import { JwtModule } from '@auth0/angular-jwt';
import { NotificationService } from './services/notification.service';
import { CommunityService } from './services/community.service';
import { PostService } from './services/post.service';
import { DialogService } from './services/dialog.service';
import { CommentsService } from './services/comments.service';
import { ImageService } from './services/image.service';
import { MessageService } from './services/message.service';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    JwtModule.forRoot({
      config: {
        tokenGetter: () => {
          const token = localStorage.getItem('token');

          return token;
        },
        whitelistedDomains: ['localhost:8080'],
      }
    }),
  ],
  providers: [
    UserService,
    AuthService,
    StorageService,
    NotificationService,
    CommunityService,
    PostService,
    DialogService,
    CommentsService,
    ImageService,
    MessageService
  ]
})
export class CoreModule { }
