import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { Message } from 'src/app/model/model.message';
import { Observable } from 'rxjs';
import { User } from 'src/app/model/model.user';

@Injectable({
  providedIn: 'root'
})
export class MessageService {

  constructor(
    private readonly db: AngularFireDatabase
  ) { }

  sendMessage(message: string, user: User, chatId: string) {
    const data = {
      senderId: user.id,
      messageBody: message,
      senderName: user.name,
      timeStamp: new Date().getTime()
    };
    this.db.list(`Chat/${chatId}/messages`).push(data);
  }

  readMessages() {
    return this.db.object('messages').valueChanges();
  }
}
