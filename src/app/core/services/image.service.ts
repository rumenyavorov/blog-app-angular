import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { JwtPayloadDTO } from 'src/app/components/login/model/model.jwt-payload';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class ImageService {
  selectedFile;
  user: JwtPayloadDTO;

  constructor(
    private readonly httpClient: HttpClient,
    private readonly authService: AuthService,
  ) { }

  public onFileChanged(event) {
    this.selectedFile = event.target.files[0];
  }

 onUpload() {
  const uploadData = new FormData();
  uploadData.append('image', this.selectedFile, this.selectedFile.name);
  this.authService.user$.subscribe(
    (userPayload) => {
      this.user = userPayload;
    }
  );

  this.httpClient.post(`http://localhost:8080/api/v1/private/user/${this.user.email}/upload`, uploadData)
  .subscribe(
    (res) => {
      console.log(res);
      this.loadImage(res);
    },
    (err: HttpErrorResponse) => {
      console.log(err.error);
    });
  }

  loadImage(res) {
    const receivedImageData = res;
    const base64Data = receivedImageData;
    const convertedImage = 'data:image/jpeg;base64,' + base64Data;

    return convertedImage;
  }
}
