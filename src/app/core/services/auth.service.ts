import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { LogInDto } from 'src/app/components/login/model/model.log-in-dto';
import { JwtHelperService } from '@auth0/angular-jwt';
import { StorageService } from './storage.service';
import { BehaviorSubject } from 'rxjs';
import { Router } from '@angular/router';
import { JwtPayloadDTO } from 'src/app/components/login/model/model.jwt-payload';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private userSubject: BehaviorSubject<JwtPayloadDTO> = new BehaviorSubject(this.isUserLogged());
  private isUserLoggedSubject: BehaviorSubject<boolean> = new BehaviorSubject(this.isLogged());

  constructor(
    private readonly http: HttpClient,
    private readonly helper: JwtHelperService,
    private readonly storageService: StorageService,
    private readonly router: Router,
  ) { }

  login(user: LogInDto) {
    return this.http.post('//localhost:8080/api/v1/public/login', user)
      .pipe(
        map(
          (data) => {
            // tslint:disable-next-line: no-string-literal
            const token = data['result'].accessToken;
            this.storageService.saveItem('token', token);
            const decodedToken = this.helper.decodeToken(token);
            this.userSubject.next({ email: decodedToken.sub, roles: decodedToken.roles });
            this.isUserLoggedSubject.next(true);
          },
          (err) => {
            console.log(err);
          }
        ),
      );
  }

  private isUserLogged() {
    const token = this.storageService.getItem('token');
    if (token !== null) {
      const decodedToken = this.helper.decodeToken(token);
      return { email: decodedToken.sub, roles: decodedToken.roles };
    } else {
      return null;
    }
  }

  private isLogged() {
    const token = this.storageService.getItem('token');
    if (token !== null) {
      return true;
    }
    return false;
  }

  public get user$() {
    return this.userSubject.asObservable();
  }

  public get isLoggedIn$() {
    return this.isUserLoggedSubject.asObservable();
  }

  public logOut() {
    this.storageService.clear();
    this.userSubject.next(null);
    this.isUserLoggedSubject.next(false);
    this.router.navigateByUrl('/');
  }

  isTokenExpired(token: string) {
    const isExpired = this.helper.isTokenExpired(token);
    return isExpired;
  }
}
