import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Post } from 'src/app/components/post/model/model.post';
import { Observable } from 'rxjs';
import { PostDto } from 'src/app/components/post/model/model.post.dto';

@Injectable({
  providedIn: 'root'
})
export class PostService {

  constructor(
    public readonly http: HttpClient,

  ) { }

  createPost(post: PostDto): Observable<Post> {
    return this.http.post<Post>('//localhost:8080/api/v1/private/post', post);
  }

  deletePost(postId: string): Observable<Post> {
    return this.http.delete<Post>('//localhost:8080/api/v1/private/post/' + postId);
  }

  updatePost(postId: string, post: PostDto): Observable<Post> {
    return this.http.put<Post>('//localhost:8080/api/v1/private/post/' + postId, post);
  }
  getAllPosts(): Observable<Post[]> {
    return this.http.get<Post[]>('//localhost:8080/api/v1/public/post');
  }

  getAllPostsByAuthor(id: string): Observable<Post[]> {
    return this.http.get<Post[]>('//localhost:8080/api/v1/public/postsByAuthor/' + id);
  }

  getAllPostsByUserJoinedCommunities(userId: string): Observable<Post[]> {
    return this.http.get<Post[]>(`//localhost:8080/api/v1/private/post/user/${userId}/communities`);
  }

  viewPost(postId: string): Observable<Post> {
    return this.http.get<Post>('//localhost:8080/api/v1/public/post/' + postId);
  }
}
