import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { RegisterUser } from 'src/app/components/register/model/model.register-user';
import { Observable } from 'rxjs';
import { User } from 'src/app/model/model.user';

@Injectable()
export class UserService {

  constructor(
    private readonly http: HttpClient,
  ) { }

  register(user: RegisterUser): Observable<User> {
    return this.http.post<User>('//localhost:8080/api/v1/public/register', user);
  }

  userProfile(): Observable<User> {
    return this.http.get<User>('//localhost:8080/api/v1/private/profile');
  }

  deleteUser(id: string): Observable<User> {
    return this.http.delete<User>('//localhost:8080/api/v1/private/admin/users/' + id);
  }

  getAllUsers(): Observable<User[]> {
    return this.http.get<User[]>('//localhost:8080/api/v1/private/admin/users');
  }

  isUserInCommunity(userEmail: string, communityName: string): Observable<boolean> {
    return this.http.get<boolean>(`//localhost:8080/api/v1/private/user/${userEmail}/community/${communityName}`);
  }

  updateUser(user: User, userId: string): Observable<User> {
    return this.http.put<User>(`//localhost:8080/api/v1/private/user/${userId}`, user);
  }

  findUserByEmail(userEmail: string): Observable<User> {
    return this.http.get<User>(`//localhost:8080/api/v1/private/user/${userEmail}`);
  }
}
