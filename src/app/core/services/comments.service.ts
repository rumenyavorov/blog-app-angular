import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CommentDto } from 'src/app/components/post/model/model.comment.dto';
import { Observable } from 'rxjs';
import { Comment } from 'src/app/components/post/model/model.comment';

@Injectable({
  providedIn: 'root'
})
export class CommentsService {

  constructor(
    private readonly http: HttpClient,
  ) { }

  createComment(postId: string, commentDto: CommentDto): Observable<Comment> {
    return this.http.post<Comment>(`//localhost:8080/api/v1/private/post/${postId}/comments`, commentDto);
  }

  findCommentsForPost(postId: string): Observable<Comment[]> {
    return this.http.get<Comment[]>(`//localhost:8080/api/v1/private/post/${postId}/comments`);
  }

  deleteComment(postId: string, commentId: string): Observable<CommentDto> {
    return this.http.delete<CommentDto>(`//localhost:8080/api/v1/private/post/${postId}/comments/${commentId}`);
  }

  findCommentsByUser(userId: string): Observable<Comment[]> {
    return this.http.get<Comment[]>(`//localhost:8080/api/v1/private/user/${userId}/comments`);
  }
}
