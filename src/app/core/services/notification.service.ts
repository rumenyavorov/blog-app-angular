import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Injectable()

export class NotificationService {
  constructor(
    private readonly toastr: ToastrService,
  ) {}

  successMessage(message: string) {
    this.toastr.success(message, '');
  }

  infoMesssage(message: string) {
    this.toastr.info(message, '');
  }

  errorMessage(message: string) {
    this.toastr.error(message, '');
  }
}
