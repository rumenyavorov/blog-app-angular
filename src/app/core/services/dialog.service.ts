import { Injectable } from '@angular/core';
import { MatDialog, MatDialogConfig, MatDialogRef } from '@angular/material/dialog';
import { ComponentType } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class DialogService {

  constructor(
    private readonly dialog: MatDialog,
  ) { }

  open(component: ComponentType<any>, config: MatDialogConfig): MatDialogRef<any> {
    const dialogRef = this.dialog.open(component, config);

    return dialogRef;
  }
}
