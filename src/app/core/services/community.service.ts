import { Injectable } from '@angular/core';
import { CommunityDto } from 'src/app/model/model.community';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JoinCommunityDto } from '../../components/community/model/join-community.dto';
import { Post } from 'src/app/components/post/model/model.post';

@Injectable({
  providedIn: 'root'
})
export class CommunityService {

  constructor(
    private readonly http: HttpClient,
  ) { }

  createCommunity(community: CommunityDto): Observable<CommunityDto> {
    return this.http.post<CommunityDto>('//localhost:8080/api/v1/private/community', community);
  }

  getAllCommunities(): Observable<CommunityDto[]> {
    return this.http.get<CommunityDto[]>('//localhost:8080/api/v1/public/community');
  }

  joinCommunity(userId: string, communityId: string): Observable<JoinCommunityDto> {
    return this.http.get<JoinCommunityDto>(`//localhost:8080/api/v1/private/community/${communityId}/user/${userId}`);
  }

  findUserJoinedCommunities(userId: string): Observable<JoinCommunityDto[]> {
    return this.http.get<JoinCommunityDto[]>(`//localhost:8080/api/v1/private/user/${userId}/communities`);
  }

  findCommunityById(communityId: string): Observable<CommunityDto> {
    return this.http.get<CommunityDto>('//localhost:8080/api/v1/private/admin/community/' + communityId);
  }

  currentUserJoinedCommunities(userId: string): Observable<Post[]> {
    return this.http.get<Post[]>(`//localhost:8080/api/v1/private/user/${userId}/communities`);
  }

  deleteCommunity(communityId: string): Observable<CommunityDto> {
    return this.http.delete<CommunityDto>('//localhost:8080/api/v1/private/admin/community/' + communityId);
  }

  joinedCommunities(userId: string): Observable<string> {
    return this.http.get<string>('//localhost:8080/api/v1/private/community/user/' + userId);
  }

  viewCommunity(communityName: string): Observable<CommunityDto> {
    return this.http.get<CommunityDto>('//localhost:8080/api/v1/public/community/' + communityName);
  }

  leaveCommunity(communityId: string): Observable<string> {
    return this.http.get<string>('//localhost:8080/api/v1/private/community/' + communityId);
  }
}
