import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { CommunityDto } from '../../model/model.community';
import { CommunityService } from '../services/community.service';

@Injectable({ providedIn: 'root' })
export class CommunitiesResolver implements Resolve<CommunityDto[]> {

  constructor(
    private readonly communityService: CommunityService,
  ) {}

  resolve(route: ActivatedRouteSnapshot): Observable<CommunityDto[]> {
    return this.communityService.getAllCommunities()
      .pipe(map(
        (communities) => {
          return communities;
        }
      ));
  }
}
