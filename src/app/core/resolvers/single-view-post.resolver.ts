import { PostService } from '../services/post.service';
import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Post } from 'src/app/components/post/model/model.post';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class SingleViewPostResolver implements Resolve<Post> {

  constructor(
    private readonly postService: PostService,
  ) {}

  resolve(route: ActivatedRouteSnapshot): Observable<Post> {
    const postId = route.params['id'];
    return this.postService.viewPost(postId)
      .pipe(map(
        (post) => {
          return post;
        }
      ));
  }
}
