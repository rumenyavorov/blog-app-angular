import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { User } from '../../model/model.user';
import { UserService } from '../services/user.service';
import { map } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class ProfileResolver implements Resolve<User> {

  constructor(
    private readonly userService: UserService,
  ) {}

  resolve(route: ActivatedRouteSnapshot): Observable<User> {
    return this.userService.userProfile()
      .pipe(map(
        (currentUser) => {
          return currentUser;
        }
      ));
  }
}
