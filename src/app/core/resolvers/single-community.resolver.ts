import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { CommunityDto } from 'src/app/model/model.community';
import { CommunityService } from '../services/community.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class SingleCommunityResolver implements Resolve<CommunityDto> {

  constructor(
    private readonly communityService: CommunityService,
  ) {}

  resolve(route: ActivatedRouteSnapshot): Observable<CommunityDto> {
    const communityName = route.paramMap.get('communityName');
    return this.communityService.viewCommunity(communityName)
      .pipe(map(
        (test) => {
          return test;
        }
      ));
  }
}
