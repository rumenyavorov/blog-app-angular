import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { UserService } from '../services/user.service';
import { map } from 'rxjs/operators';
import { AuthService } from '../services/auth.service';
import { JwtPayloadDTO } from 'src/app/components/login/model/model.jwt-payload';
import { User } from 'src/app/model/model.user';

@Injectable({ providedIn: 'root' })
export class IsUserInCommunityResolver implements Resolve<boolean> {
  user: JwtPayloadDTO;
  constructor(
    private readonly userService: UserService,
    private readonly authService: AuthService,
  ) {}

  resolve(route: ActivatedRouteSnapshot): Observable<boolean> {
    this.authService.user$.subscribe(
      (currentUser: JwtPayloadDTO) => {
        this.user = currentUser;
      }
    );
    const communityName = route.params['communityName'];

    if (this.user === null) {
      return;
    } else {
      return this.userService.isUserInCommunity(this.user.email, communityName)
      .pipe(map(
        (isUserInCommunity) => {
          return isUserInCommunity;
        }
      ));
    }
  }
}
