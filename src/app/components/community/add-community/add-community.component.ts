import { Component, OnInit } from '@angular/core';
import { CommunityService } from 'src/app/core/services/community.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NotificationService } from 'src/app/core/services/notification.service';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-add-community',
  templateUrl: './add-community.component.html',
  styleUrls: ['./add-community.component.css']
})
export class AddCommunityComponent implements OnInit {
  communityForm: FormGroup;
  constructor(
    private readonly communityService: CommunityService,
    private readonly formBuilder: FormBuilder,
    private readonly notificationService: NotificationService,
    private readonly router: Router,
  ) { }

  ngOnInit() {
    this.communityForm = this.formBuilder.group({
      name: ['', [Validators.required, Validators.minLength(3)]],
      description: ['', [Validators.required, Validators.minLength(3)]]
    });
  }


  addCommunity() {
    if (!this.communityForm.valid) {
      return;
    }
    this.communityService.createCommunity(this.communityForm.value).subscribe(
      (_) => {
        this.notificationService.successMessage('Community created');
        this.router.navigateByUrl('/');
      },
      (err: HttpErrorResponse) => {
        if (err.error.message === 'Community already exist!') {
          this.notificationService.errorMessage(err.error.message);
        } else {
          this.notificationService.errorMessage('Server error!');
        }
      }
      );
  }
}
