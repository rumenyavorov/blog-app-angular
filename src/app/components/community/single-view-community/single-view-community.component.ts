import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CommunityService } from 'src/app/core/services/community.service';
import { CommunityDto } from 'src/app/model/model.community';
import { AuthService } from 'src/app/core/services/auth.service';
import { PostDto } from '../../post/model/model.post.dto';
import { JwtPayloadDTO } from '../../login/model/model.jwt-payload';
import { UserService } from 'src/app/core/services/user.service';
import { User } from 'src/app/model/model.user';
import { NotificationService } from 'src/app/core/services/notification.service';
import { HttpErrorResponse } from '@angular/common/http';
import { ImageService } from 'src/app/core/services/image.service';

@Component({
  selector: 'app-single-view-community',
  templateUrl: './single-view-community.component.html',
  styleUrls: ['./single-view-community.component.css']
})
export class SingleViewCommunityComponent implements OnInit {
  singleCommunity: CommunityDto;
  singleCommunityPosts: PostDto[];
  loggedIn: boolean;
  user: JwtPayloadDTO;
  userInformation: User;
  isUserJoined: boolean;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly communityService: CommunityService,
    private readonly router: Router,
    private readonly authService: AuthService,
    private readonly userService: UserService,
    private readonly notificationService: NotificationService,
    private readonly imageService: ImageService
  ) { }

  ngOnInit(): void {
    this.route.data.subscribe(({ singleViewCommunity }) => {
      this.singleCommunity = singleViewCommunity;
      // console.log(this.singleCommunity.communityImage.image);
      this.singleCommunityPosts = singleViewCommunity.posts;
    });

    this.authService.user$.subscribe((user) => {
      this.user = user;
      if (user !== null) {
        this.getUserInformation();
        this.route.data.subscribe(({ isUserJoined }) => {
          this.isUserJoined = isUserJoined;
        });
      }
    });
  }

  getUserInformation() {
    this.userService.userProfile().subscribe(
      (userInformation) => {
        this.userInformation = userInformation;
      }
    );
  }

  createPostFromCommunity() {
    this.router.navigateByUrl('/add-post');
  }

  joinCommunity(communityId: string) {
    this.communityService.joinCommunity(this.userInformation.id, communityId).subscribe(
      (_) => {
        this.notificationService.successMessage('Community joined.');
      },
      (err: HttpErrorResponse) => {
        this.notificationService.infoMesssage('Community already joined');
      }
    );
  }

  leaveCommunity(communityId: string) {
    this.communityService.leaveCommunity(communityId).subscribe(
      (_) => {
        this.notificationService.successMessage('Community left.');
      }
    );
  }

  viewPost(postId: string) {
    if (postId === null) {
      this.notificationService.errorMessage('Post doesn\'t exist');
    } else {
      this.router.navigateByUrl('/post/' + postId);
    }
  }

  loadImage(image: string) {
    return this.imageService.loadImage(image);
  }
}
