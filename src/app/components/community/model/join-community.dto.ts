export class JoinCommunityDto {
  userId: string;
  communityId: string;
}
