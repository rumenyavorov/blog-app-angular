import { Component, OnInit } from '@angular/core';
import { CommunityService } from 'src/app/core/services/community.service';
import { CommunityDto } from 'src/app/model/model.community';
import { PostService } from 'src/app/core/services/post.service';
import { Post } from '../post/model/model.post';
import { HttpErrorResponse } from '@angular/common/http';
import { AuthService } from 'src/app/core/services/auth.service';
import { JwtPayloadDTO } from '../login/model/model.jwt-payload';
import { UserService } from 'src/app/core/services/user.service';
import { User } from 'src/app/model/model.user';
import { Router } from '@angular/router';
import { NotificationService } from 'src/app/core/services/notification.service';
import { MatTableDataSource } from '@angular/material/table';
import { ImageService } from 'src/app/core/services/image.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  communities: CommunityDto[];
  posts: Post[];
  user: JwtPayloadDTO;
  userInformation: User;
  communitiesJoined: CommunityDto[];
  postsByJoinedCommunities: Post[];
  test: string[];
  res: any;
  leaveJoinedCommunity = false;
  dataSource: any;
  constructor(
    private readonly communityService: CommunityService,
    private readonly postService: PostService,
    private readonly authService: AuthService,
    private readonly userService: UserService,
    private readonly router: Router,
    private readonly notificationService: NotificationService,
    private readonly imageService: ImageService,
    ) { }

  ngOnInit() {
    this.authService.user$.subscribe((user) => {
      this.user = user;
      if (user !== null) {
        this.getUserInformation();
      }
    });
    this.getAllCommunities();
    this.getAllPosts();
  }

  getAllCommunities() {
    this.communityService.getAllCommunities().subscribe(
      (communities) => {
        this.communities = communities;
    });
  }

  loadImage(image: string) {
    return this.imageService.loadImage(image);
  }

  getAllPosts() {
    this.postService.getAllPosts().subscribe(
      (posts) => {
        this.posts = posts;
      },
      (err: HttpErrorResponse) => {
        console.log(err);
      });
  }

  deletePost(postId: string) {
    this.postService.deletePost(postId).subscribe(
      (_) => {
        this.getAllCommunities();
      });
  }

  getUserInformation() {
    this.userService.userProfile().subscribe(
      (userInformation) => {
        this.userInformation = userInformation;
        this.findUserJoinedCommunities();
      }
    );
  }

  findUserJoinedCommunities() {
    this.postService.getAllPostsByUserJoinedCommunities(this.userInformation.id).subscribe(
      (postsByJoinedCommunities) => {
        this.postsByJoinedCommunities = postsByJoinedCommunities;
      }
    );
  }

  viewCommunity(communityName: string) {
    if (communityName === null) {
      this.notificationService.errorMessage('Community doesn\'t exist');
    } else {
      this.router.navigateByUrl('/community/' + communityName);
    }
  }

  viewPost(postId: string) {
    if (postId === null) {
      this.notificationService.errorMessage('Post doesn\'t exist');
    } else {
      this.router.navigateByUrl('/post/' + postId);
    }
  }
}
