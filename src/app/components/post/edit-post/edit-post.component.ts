import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PostService } from 'src/app/core/services/post.service';
import { NotificationService } from 'src/app/core/services/notification.service';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Post } from '../model/model.post';

@Component({
  selector: 'app-edit-post',
  templateUrl: './edit-post.component.html',
  styleUrls: ['./edit-post.component.css']
})
export class EditPostComponent implements OnInit {
  editPostForm: FormGroup;
  constructor(
    private readonly formBuilder: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: { post: Post },
    private readonly dialogRef: MatDialogRef<EditPostComponent>,
  ) { }

  ngOnInit() {
    console.log(this.data);
    this.editPostForm = this.formBuilder.group({
      title: [this.data.post.title, [Validators.minLength(12), Validators.maxLength(100)]],
      body: [this.data.post.body, [Validators.minLength(20), Validators.maxLength(1000)]],
      community: [this.data.post.community]
    });
  }

  editPost() {
    this.dialogRef.close({...this.editPostForm.value, id: this.data.post.id});
  }

  onCancel() {
    this.dialogRef.close();
  }
}
