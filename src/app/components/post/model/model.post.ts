import { User } from '../../../model/model.user';
import { CommunityDto } from '../../../model/model.community';
import { Image } from 'src/app/model/model.image';

export class Post {
  id: string;
  title: string;
  body: string;
  author: User;
  community: CommunityDto;
  comments: Comment[];
  postImage: Image;
}
