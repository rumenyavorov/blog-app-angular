import { User } from 'src/app/model/model.user';
import { Post } from './model.post';

export class Comment {
  commentId: string;
  commentBody: string;
  user: User;
  post: Post;
  date: string;
}
