import { CommunityDto } from '../../../model/model.community';
import { User } from 'src/app/model/model.user';

export class PostDto {
  id: string;
  title: string;
  body: string;
  author: User;
  community: CommunityDto;
}
