import { Component, OnInit } from '@angular/core';
import { PostService } from 'src/app/core/services/post.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { CommunityService } from 'src/app/core/services/community.service';
import { CommunityDto } from 'src/app/model/model.community';
import { Post } from '../model/model.post';
import { NotificationService } from 'src/app/core/services/notification.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-post',
  templateUrl: './create-post.component.html',
  styleUrls: ['./create-post.component.css']
})
export class CreatePostComponent implements OnInit {
  communities: CommunityDto[];
  postForm: FormGroup;
  constructor(
    private readonly postService: PostService,
    private readonly formBuilder: FormBuilder,
    private readonly communityService: CommunityService,
    private readonly notificationService: NotificationService,
    private readonly router: Router,
  ) { }

  ngOnInit() {
    this.postForm = this.formBuilder.group({
      title: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(100)]],
      body: ['', [Validators.required, Validators.minLength(20), Validators.maxLength(1000)]],
      community: ['', [Validators.required]]
    });

    this.getAllCommunities();
  }

  createPost() {
    if (!this.postForm.valid) {
      return;
    }
    this.postService.createPost(this.postForm.value).subscribe(
      (_) => {
        this.notificationService.successMessage('Post created successfully');
        this.router.navigateByUrl('/');
      },
      (err: HttpErrorResponse) => {
        this.notificationService.errorMessage(err.error.message);
      });
  }

  getAllCommunities() {
    this.communityService.getAllCommunities().subscribe(
      (communities) => {
        this.communities = communities;
      });
  }
}
