import { Component, OnInit } from '@angular/core';
import { PostService } from 'src/app/core/services/post.service';
import { CommentsService } from 'src/app/core/services/comments.service';
import { ActivatedRoute } from '@angular/router';
import { Post } from '../model/model.post';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'src/app/core/services/auth.service';
import { NotificationService } from 'src/app/core/services/notification.service';
import { HttpErrorResponse } from '@angular/common/http';
import { Comment } from '../model/model.comment';
import { User } from 'src/app/model/model.user';
import * as moment from 'moment';
import { ImageService } from 'src/app/core/services/image.service';

@Component({
  selector: 'app-single-view-post',
  templateUrl: './single-view-post.component.html',
  styleUrls: ['./single-view-post.component.css']
})
export class SingleViewPostComponent implements OnInit {
  singlePost: Post;
  postComments: Comment[];
  commentForm: FormGroup;
  loggedIn: boolean;
  currentUser: User;
  constructor(
    private readonly authService: AuthService,
    private readonly commentService: CommentsService,
    private readonly route: ActivatedRoute,
    private readonly formBuilder: FormBuilder,
    private readonly notificationService: NotificationService,
    private readonly imageService: ImageService
  ) { }

  ngOnInit(): void {
    this.authService.user$.subscribe(
      (user: User) => {
        this.currentUser = user;
      }
    );
    this.route.data.subscribe(({ singlePost }) => {
      this.singlePost = singlePost;
      this.postComments = singlePost.comments;
      this.postComments.reverse();
    });
    this.commentForm = this.formBuilder.group({
      commentBody: ['', [Validators.required, Validators.minLength(3)]]
    });
  }


  loadImage(image: string) {
    return this.imageService.loadImage(image);
  }

  difference(dateTime: string) {
    return moment(dateTime).fromNow();
  }

  createComment() {
    if (!this.commentForm.valid) {
      return;
    }
    this.commentService.createComment(this.singlePost.id, this.commentForm.value).subscribe(
      (comment: Comment) => {
        if (comment) {
          this.postComments.push(comment);
          this.postComments.unshift(this.postComments.pop());
          this.notificationService.successMessage('Comment added.');
        }
      },
        (err: HttpErrorResponse) => {
          console.log(err);
      }
    );
  }

  deleteComment(postId: string, commentId: string) {
    this.commentService.deleteComment(postId, commentId).subscribe(
      (res) => {
        if (res) {
          this.postComments = this.postComments.filter(comment => comment.commentId !== commentId);
          this.notificationService.successMessage('Comment deleted.');
        }
      }, (_) => {
        this.notificationService.errorMessage('Comment doesn\'t exist!');
      }
    );
  }
}
