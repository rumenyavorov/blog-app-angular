export class JwtPayloadDTO {
  email: string;
  roles: string[];
}
