import { Component, OnInit } from '@angular/core';
import { Message } from 'src/app/model/model.message';
import { Observable } from 'rxjs';
import { MessageService } from 'src/app/core/services/message.service';
import { AngularFireDatabase } from '@angular/fire/database';
import { AuthService } from 'src/app/core/services/auth.service';
import { JwtPayloadDTO } from '../login/model/model.jwt-payload';
import { UserService } from 'src/app/core/services/user.service';
import { User } from 'src/app/model/model.user';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {
  loggedUser: User;
  messages: Message;
  chatForm: FormGroup;

  constructor(
    private readonly messageService: MessageService,
    private readonly db: AngularFireDatabase,
    private readonly userService: UserService,
    private readonly formBuider: FormBuilder,
  ) { }

  ngOnInit(): void {
    this.chatForm = this.formBuider.group({
      message: ['']
    });
    const test = this.db.object('messages').valueChanges();
    this.userService.userProfile().subscribe(
      (user: User) => {
        this.loggedUser = user;
      }
    );
  }

  sendMessage() {
    this.messageService.sendMessage(this.chatForm.value, this.loggedUser, this.loggedUser.id);
  }

  returnMessages() {
    this.db.object(this.loggedUser.id).valueChanges().subscribe(
      (message: Message) => {
        console.log(message);
      }
    );
  }
}
