import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/core/services/user.service';
import { Router } from '@angular/router';
import { RegisterUser } from 'src/app/components/register/model/model.register-user';
import { HttpErrorResponse } from '@angular/common/http';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { matchingPasswords } from 'src/app/common/validator/matchingPasswords.validator';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup;
  confirmPasswordCheck: boolean;
  errorMessage: string;
  constructor(
    private readonly userService: UserService,
    private readonly router: Router,
    private readonly formBuilder: FormBuilder,
    private readonly toastr: ToastrService,
  ) { }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]],
      name: ['', Validators.required],
      password: ['', [Validators.required, Validators.minLength(6)]],
      confirmPassword: ['', [Validators.required, Validators.minLength(6)]]
    }, {
      validators: [matchingPasswords('password', 'confirmPassword')]
    });
  }

  register() {
    if (this.registerForm.invalid) {
      return;
    }
    this.userService.register(this.registerForm.value).subscribe(
      (response) => {
        this.toastr.success('', response['message']);
        this.router.navigateByUrl('/login');
      }, (err: HttpErrorResponse) => {
        this.toastr.error('', err.error.message);
        console.log(err.error);
      }
    );
  }
}
