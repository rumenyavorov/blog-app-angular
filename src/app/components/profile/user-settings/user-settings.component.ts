import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { AuthService } from 'src/app/core/services/auth.service';
import { JwtPayloadDTO } from '../../login/model/model.jwt-payload';
import { ImageService } from 'src/app/core/services/image.service';

@Component({
  selector: 'app-user-settings',
  templateUrl: './user-settings.component.html',
  styleUrls: ['./user-settings.component.css']
})
export class UserSettingsComponent implements OnInit {

  constructor(
    private readonly httpClient: HttpClient,
    private readonly imageService: ImageService,
  ) { }

  ngOnInit(): void {
  }

  onFileChange(event) {
    this.imageService.onFileChanged(event);
  }

  uploadProfilePicture() {
    this.imageService.onUpload();
  }
}
