import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-profile-default',
  templateUrl: './profile-default.component.html',
  styleUrls: ['./profile-default.component.css']
})
export class ProfileDefaultComponent implements OnInit {
  user: any;
    constructor(
    private readonly route: ActivatedRoute,
    ) { }

  ngOnInit() {
  }

  toggleNav(sidenav: any) {
    if (sidenav.opened) {
      sidenav.close();
    } else {
      sidenav.open();
    }
  }
}
