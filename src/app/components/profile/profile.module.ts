import { NgModule } from '@angular/core';
import { UsersComponent } from './admin/dashboard/users/users.component';
import { ProfileDefaultComponent } from './profile-default/profile-default.component';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { PostsByUserComponent } from './posts-by-user/posts-by-user.component';
import { MaterialModule } from 'src/app/modules/material.module';
import { AddCommunityComponent } from '../community/add-community/add-community.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ProfileStatsComponent } from './profile-stats/profile-stats.component';
import { CommunitiesComponent } from './admin/communities/communities.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { UserSettingsComponent } from './user-settings/user-settings.component';
import { EditUserComponent } from './admin/dashboard/edit-user/edit-user.component';

@NgModule({
  declarations: [
    UsersComponent,
    ProfileDefaultComponent,
    PostsByUserComponent,
    AddCommunityComponent,
    ProfileStatsComponent,
    CommunitiesComponent,
    UserSettingsComponent,
    EditUserComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    SharedModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule
  ],
  exports: [
    UsersComponent,
    ProfileDefaultComponent,
    PostsByUserComponent,
    AddCommunityComponent,
    CommunitiesComponent
  ]
})
export class ProfileModule { }
