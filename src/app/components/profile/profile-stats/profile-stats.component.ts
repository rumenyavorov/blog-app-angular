import { Component, OnInit } from '@angular/core';
import { PostService } from 'src/app/core/services/post.service';
import { ActivatedRoute } from '@angular/router';
import { User } from 'src/app/model/model.user';
import { Post } from '../../post/model/model.post';
import { CommunityService } from 'src/app/core/services/community.service';
import { CommentsService } from 'src/app/core/services/comments.service';
import { Comment } from '../../post/model/model.comment';

@Component({
  selector: 'app-profile-stats',
  templateUrl: './profile-stats.component.html',
  styleUrls: ['./profile-stats.component.css']
})
export class ProfileStatsComponent implements OnInit {
  currentUser;
  posts: Post[];
  communitiesJoined: string;
  commentsByUser: Comment[];
  constructor(
    private readonly route: ActivatedRoute,
    private readonly postService: PostService,
    private readonly communityService: CommunityService,
    private readonly commentService: CommentsService
  ) { }

  ngOnInit() {
    this.route.data.subscribe(
      (currentUser) => {
        this.currentUser = currentUser;
        this.loadPostsStats();
        this.loadJoinedCommunities();
        this.findCommentsByUser();
      }
    );
  }

  loadPostsStats() {
    this.postService.getAllPostsByAuthor(this.currentUser.currentUser.id).subscribe(
      (posts) => {
        this.posts = posts;
      }
    );
  }

  loadJoinedCommunities() {
    this.communityService.joinedCommunities(this.currentUser.currentUser.id).subscribe(
      (communitiesJoined) => {
        this.communitiesJoined = communitiesJoined;
      }
    );
  }

  findCommentsByUser() {
    this.commentService.findCommentsByUser(this.currentUser.currentUser.id).subscribe(
      (comments: Comment[]) => {
        this.commentsByUser = comments;
      }
    );
  }
}
