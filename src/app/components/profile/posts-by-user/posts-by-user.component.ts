import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { PostService } from 'src/app/core/services/post.service';
import { ActivatedRoute } from '@angular/router';
import { User } from 'src/app/model/model.user';
import { Post } from '../../post/model/model.post';
import { NotificationService } from 'src/app/core/services/notification.service';
import { HttpErrorResponse } from '@angular/common/http';
import { DialogService } from 'src/app/core/services/dialog.service';
import { EditPostComponent } from '../../post/edit-post/edit-post.component';

@Component({
  selector: 'app-posts-by-user',
  templateUrl: './posts-by-user.component.html',
  styleUrls: ['./posts-by-user.component.css']
})
export class PostsByUserComponent implements OnInit {
  user: User;
  posts: Post[];
  constructor(
    private readonly postService: PostService,
    private readonly route: ActivatedRoute,
    private readonly notificationService: NotificationService,
    private readonly dialogService: DialogService,
    ) { }

  ngOnInit() {
    this.route.data.subscribe(({ currUser }) => {
      this.user = currUser;
      this.getPostsByUser();
     });
  }

  getPostsByUser() {
    this.postService.getAllPostsByAuthor(this.user.id).subscribe(
      (posts) => {
        this.posts = posts;
        console.log(this.posts.length);
      });
  }

  editPost(post: Post) {
    const dialogRef = this.dialogService.open(EditPostComponent, { data: { post } });
    dialogRef.afterClosed().subscribe(
      (res) => {
        if (res) {
          this.postService.updatePost(res.id, res).subscribe(
            (msg) => {
              this.notificationService.successMessage(msg['message']);
              this.posts.map(e => {
                if (e.id === res.id) {
                  e.title = res.title;
                  e.body = res.body;
                  e.community = res.community;
                }
                return e;
              });
            });
        }
      }
    );
  }

  deletePost(id: string) {
    this.postService.deletePost(id).subscribe(
      (res) => {
        this.notificationService.successMessage(res['mesage']);
      },
    );
  }
}
