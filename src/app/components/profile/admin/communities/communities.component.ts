import { Component, OnInit } from '@angular/core';
import { CommunityService } from 'src/app/core/services/community.service';
import { CommunityDto } from 'src/app/model/model.community';
import { ActivatedRoute, Router } from '@angular/router';
import { MatTableDataSource } from '@angular/material/table';
import { NotificationService } from 'src/app/core/services/notification.service';

@Component({
  selector: 'app-communities',
  templateUrl: './communities.component.html',
  styleUrls: ['./communities.component.css']
})
export class CommunitiesComponent implements OnInit {
  communities: CommunityDto[];
  dataSource: any;
  displayedColumns: string[] = ['id', 'name', 'posts', 'users', 'actions'];
  constructor(
    private readonly communityService: CommunityService,
    private readonly route: ActivatedRoute,
    private readonly notificationService: NotificationService,
    private readonly router: Router,
  ) { }

  ngOnInit() {
    this.route.data.subscribe(({ communities }) => {
      this.communities = communities;
      console.log(this.communities);
    });
    this.dataSource = new MatTableDataSource(this.communities);
  }

  deleteCommunity(communityId: string) {
    this.communityService.deleteCommunity(communityId).subscribe(
      (res) => {
        this.notificationService.successMessage(res['message']);
      }
    );
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  goToSingleCommunity(communityName: string) {
    this.router.navigateByUrl('/community/' + communityName);
  }
}
