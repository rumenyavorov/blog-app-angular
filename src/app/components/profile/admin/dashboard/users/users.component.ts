import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserService } from 'src/app/core/services/user.service';
import { User } from 'src/app/model/model.user';
import { MatTableDataSource } from '@angular/material/table';
import { NotificationService } from 'src/app/core/services/notification.service';
import { HttpErrorResponse } from '@angular/common/http';
import { timeout, delay } from 'rxjs/operators';
import { DialogService } from 'src/app/core/services/dialog.service';
import { EditUserComponent } from '../edit-user/edit-user.component';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  users: User[];
  displayedColumns: string[] = ['id', 'name', 'email', 'role', 'date', 'actions'];
  dataSource: any;

  constructor(
    private readonly userService: UserService,
    private readonly route: ActivatedRoute,
    private readonly notificationService: NotificationService,
    private readonly dialogService: DialogService,
  ) { }

  ngOnInit() {
    this.route.data.subscribe(({ users }) => {
      this.users = users;
      this.dataSource = new MatTableDataSource(this.users);
    });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  getAllUsers() {
    this.userService.getAllUsers().subscribe(
      (users) => {
        this.users = users;
      }
    );
  }

  editUser(user: User) {
    const dialogRef = this.dialogService.open(EditUserComponent, { data: { user }});
    dialogRef.afterClosed().subscribe(
      (userRes) => {
        if (userRes) {
          this.userService.updateUser(userRes, userRes.userId).subscribe(
            (msg) => {
              this.notificationService.successMessage('User update successfully.');
              this.users.map(e => {
                if (e.id === userRes.userId) {
                  e.name = userRes.name;
                  e.email = userRes.email;
                }
              });
            }
          );
        }
      }
    );
  }

  deleteUser(id: string) {
    this.userService.deleteUser(id).subscribe(
      (res) => {
        this.notificationService.successMessage(res['message']);
        delay(500),
        this.getAllUsers();
      },
      (err: HttpErrorResponse) => {
        this.notificationService.errorMessage(err.error.message);
      });
  }
}
