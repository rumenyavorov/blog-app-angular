import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { User } from 'src/app/model/model.user';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css']
})
export class EditUserComponent implements OnInit {
  editUserForm: FormGroup;
  constructor(
    private readonly formBuilder: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: { user: User },
    private readonly dialogRef: MatDialogRef<EditUserComponent>,
  ) { }

  ngOnInit(): void {
    this.editUserForm = this.formBuilder.group({
      name: [this.data.user.name],
      email: [this.data.user.email],
    });
  }

  editUser() {
    this.dialogRef.close({...this.editUserForm.value, userId: this.data.user.id});
  }

  onCancel() {
    this.dialogRef.close();
  }

}
