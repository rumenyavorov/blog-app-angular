import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/core/services/auth.service';
import { User } from 'src/app/model/model.user';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {
  loggedIn: boolean;
  currentUser: any;
  constructor(
    private readonly router: Router,
    private readonly authService: AuthService,
  ) { }

  ngOnInit() {
    this.authService.isLoggedIn$.subscribe((loggedIn) => {
      this.loggedIn = loggedIn;
    });
    this.authService.user$.subscribe((currentUser) => {
      this.currentUser = currentUser;
    });
  }

  logOut() {
    this.authService.logOut();
    this.router.navigateByUrl('/');
  }
}
