import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/core/services/auth.service';
import { User } from 'src/app/model/model.user';
import { ActivatedRoute } from '@angular/router';
import { ImageService } from 'src/app/core/services/image.service';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.css']
})
export class SidenavComponent implements OnInit {
  user: User;
  convertedImage: any;
  base64Data: any;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly imageService: ImageService,
  ) { }

  ngOnInit() {
    this.route.data.subscribe(({ currentUser }) => {
      this.user = currentUser;
      if (this.user.userProfilePicture.image === null) {
        return false;
      } else {
        this.convertedImage = this.imageService.loadImage(this.user.userProfilePicture.image);
      }
    });
  }
}
