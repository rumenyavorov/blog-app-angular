import { CommunityDto } from './model.community';
import { Image } from '../model/model.image';

export class User {
  id: string;
  email: string;
  name: string;
  roles: any;
  dateAdded: string;
  communities: CommunityDto[];
  userProfilePicture: Image;
}
