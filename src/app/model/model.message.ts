import { User } from './model.user';

export class Message {
  message: string;
  author: string;
}
