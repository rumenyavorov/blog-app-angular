import { Post } from '../components/post/model/model.post';
import { User } from './model.user';
import { Image } from './model.image';

export class CommunityDto {
  id: string;
  name: string;
  description: string;
  posts: Post[];
  author: User;
  communityImage: Image;
}
