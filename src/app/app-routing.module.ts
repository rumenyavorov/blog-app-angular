import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RegisterComponent } from './components/register/register.component';
import { LoginComponent } from './components/login/login.component';
import { HomeComponent } from './components/home/home.component';
import { CreatePostComponent } from './components/post/create-post/create-post.component';
import { AdminGuard } from './core/guards/admin.guard';
import { NotFoundComponent } from './shared/components/not-found/not-found.component';
import { ProfileDefaultComponent } from './components/profile/profile-default/profile-default.component';
import { UsersComponent } from './components/profile/admin/dashboard/users/users.component';
import { PostsByUserComponent } from './components/profile/posts-by-user/posts-by-user.component';
import { ProfileResolver } from './core/resolvers/profile.resolver';
import { UsersResolver } from './core/resolvers/users.resolver';
import { AddCommunityComponent } from './components/community/add-community/add-community.component';
import { EditPostComponent } from './components/post/edit-post/edit-post.component';
import { CommunitiesComponent } from './components/profile/admin/communities/communities.component';
import { CommunitiesResolver } from './core/resolvers/communities.resolver';
import { ProfileStatsComponent } from './components/profile/profile-stats/profile-stats.component';
import { SingleViewCommunityComponent } from './components/community/single-view-community/single-view-community.component';
import { SingleViewPostComponent } from './components/post/single-view-post/single-view-post.component';
import { IsUserInCommunityResolver } from './core/resolvers/is-user-in-community.resolver';
import { SingleCommunityResolver } from './core/resolvers/single-community.resolver';
import { SingleViewPostResolver } from './core/resolvers/single-view-post.resolver';
import { UserSettingsComponent } from './components/profile/user-settings/user-settings.component';
import { ChatComponent } from './components/chat/chat.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'register',
    component:
    RegisterComponent
  },
  {
    path: 'login',
    component:
    LoginComponent
  },
  {
    path: 'add-post',
    component:
    CreatePostComponent
  },
  {
    path: 'not-found',
    component:
    NotFoundComponent
  },
  {
    path: 'profile',
    component: ProfileDefaultComponent,
    resolve: { currentUser: ProfileResolver },
    children: [{
      path: 'users',
      component: UsersComponent,
      canActivate: [AdminGuard],
      resolve: { users: UsersResolver },
    }, {
      path: 'posts',
      component: PostsByUserComponent,
      resolve: { currUser: ProfileResolver }
    },
    {
      path: 'create-community',
      component: AddCommunityComponent,
    },
    {
      path: 'communities',
      component: CommunitiesComponent,
      canActivate: [AdminGuard],
      resolve: { communities: CommunitiesResolver }
    },
    {
      path: '',
      component: ProfileStatsComponent,
    },
    {
      path: 'settings',
      component: UserSettingsComponent
    }
  ]
  },
  { path: 'posts/edit/:id', component: EditPostComponent},
  {
    path: 'community/:communityName',
    component: SingleViewCommunityComponent,
    resolve: { isUserJoined: IsUserInCommunityResolver, singleViewCommunity: SingleCommunityResolver }
  },
  {
    path: 'post/:id',
    component: SingleViewPostComponent,
    resolve: { singlePost: SingleViewPostResolver }
  },
  {
    path: 'chat', component: ChatComponent
  },
  { path: '**', redirectTo: '/not-found' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
